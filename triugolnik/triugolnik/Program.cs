﻿using System;

namespace triugolnik
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Double a, b, c, p, s;
            Console.WriteLine("a = ");
            a = Double.Parse(Console.ReadLine());
            Console.WriteLine("b = ");
            b = Double.Parse(Console.ReadLine());
            Console.WriteLine("c =  ");
            c = Double.Parse(Console.ReadLine());
            p = a + b + c;
            s = Math.Sqrt(p / 2 * (p / 2 - a) * (p / 2 - b) * (p / 2 - c));
            Console.WriteLine($"Периметр равен: {p}");
            Console.WriteLine($"Площадь равна: {s}");
        }
    }
}
