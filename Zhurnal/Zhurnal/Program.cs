﻿using System;

namespace Zhurnal
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Цифровая модель журнала");

            Double Height, Length, Width, Mass, Volume, OnePageMass;
            int PagesCount, TotalWordCount, WordsOnOnePage;

            Console.WriteLine("Height = ");
            string HeightStr = Console.ReadLine();
            Height = Convert.ToDouble(HeightStr);

            Console.WriteLine("Length = ");
            string LengthStr = Console.ReadLine();
            Length = Convert.ToDouble(LengthStr);

            Console.WriteLine("Width = ");
            string WidthStr = Console.ReadLine();
            Width = Convert.ToDouble(WidthStr);

            Console.WriteLine("Pages count = ");
            string PagesCountStr = Console.ReadLine();
            PagesCount = Convert.ToInt32(PagesCountStr);

            Console.WriteLine("WordsOnOnePage = ");
            string WordsOnOnePageStr = Console.ReadLine();
            WordsOnOnePage = Convert.ToInt32(WordsOnOnePageStr);

            Console.WriteLine("One Page mass = ");
            string OnePageMassStr = Console.ReadLine();
            OnePageMass = Convert.ToInt32(OnePageMassStr);

            //вычисляем

            Mass = OnePageMass * PagesCount;
            Volume = Height * Length * Width;
            TotalWordCount = WordsOnOnePage * PagesCount;

            //вывод

            Console.WriteLine("Height: " + Height + " cm");
            Console.WriteLine("Length: " + Length + " cm");
            Console.WriteLine("Width: " + Width + " cm");
            Console.WriteLine("Volume: " + Volume + "cm^3");
            Console.WriteLine("PagesCount: " + PagesCount);
            Console.WriteLine("WordsOnOnePage: " + WordsOnOnePage);
            Console.WriteLine("TotalWordCount: " + TotalWordCount);
            Console.WriteLine("OnePageMass: " + OnePageMass + " g");
            Console.WriteLine("Mass: " + Mass + " g");

        }
    }
}
