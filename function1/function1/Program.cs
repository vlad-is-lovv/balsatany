﻿using System;

namespace function1
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            double f, x;
            Console.WriteLine("x = ");
            x = Double.Parse(Console.ReadLine());
            f = 1/43 + Math.Log (Math.Abs (3*x/4)) + x/7 + Math.Min(x,-3) + (Math.Cos(x/3)/Math.Sin(x/3)) + Math.Pow(x,x);
            Console.WriteLine($"F(x) = {f}");
        }
    }
}
