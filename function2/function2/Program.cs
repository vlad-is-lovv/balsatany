﻿using System;

namespace function2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Double f, x, y, z;
            Console.WriteLine("x =");
            x = Double.Parse(Console.ReadLine());
            Console.WriteLine("y =");
            y = Double.Parse(Console.ReadLine());
            Console.WriteLine("z =");
            z = Double.Parse(Console.ReadLine());
            f = 1 / 43 + Math.Log(Math.Abs(y * 3 / 4)) + z / 7 + Math.Min(x, -3);
            Console.WriteLine($"F(x,y,z) = {f}");

        }
    }
}
